module.exports.routes = {

    /* -- Pages -- */
    '/': {
        controller: 'HomeController',
        action: 'index'
    },

    '/the-seats': {
        controller: 'SeatsController',
        action: 'index'
    },

    '/how-it-works': {
        controller: 'HowController',
        action: 'index'
    },

    '/vip-parking': {
        controller: 'ParkingController',
        action: 'index'
    },

    /* -- Games -- */
    'get /games/:id/edit': 'GamesController.edit',
    'post /games/:id/update': 'GamesController.update',
    'post /games/create': 'GamesController.create',
    'post /games/request': 'GamesController.request',

    /* -- Stats -- */
    'get /stats/standings': 'StatsController.standings',
    'get /stats/standings/get': 'StatsController.get',
    'get /stats/standings/refresh': 'StatsController.refresh',

    /* -- User -- */

    'get /login': 'AuthController.login',
    'get /logout': 'AuthController.logout',
    'get /register': 'AuthController.register',

    'post /auth/local': 'AuthController.callback',
    'post /auth/local/:action': 'AuthController.callback',

    'get /auth/:provider': 'AuthController.provider',
    'get /auth/:provider/callback': 'AuthController.callback',

    'post /user/update': 'UserController.update',

    /* -- Images -- */
    'get /images/random': 'ImagesController.random',
    'get /images/all': 'ImagesController.all'

};
