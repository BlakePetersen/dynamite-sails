/**
 * Passport configuration
 *
 * This if the configuration for your Passport.js setup and it where you'd
 * define the authentication strategies you want your application to employ.
 *
 * I have tested the service with all of the providers listed below - if you
 * come across a provider that for some reason doesn't work, feel free to open
 * an issue on GitHub.
 *
 * Also, authentication scopes can be set through the `scope` property.
 *
 * For more information on the available providers, check out:
 * http://passportjs.org/guide/providers/
 */

module.exports.passport = {
    local: {
        strategy: require('passport-local').Strategy
    },

    twitter: {
        name: 'Twitter',
        protocol: 'oauth',
        strategy: require('passport-twitter').Strategy,
        options: {
            consumerKey: 'noUs3J9Hwzf6GekL6hrfc38ua',
            consumerSecret: 'CEznJ1iXKKrLNc68db2TA0Yjwn1xBDBXjDBYB1ZH4GHQxfMBUF',
            callbackURL: 'http://local.dynamiteseats.com/auth/twitter/callback'
        }
    },

    github: {
        name: 'GitHub',
        protocol: 'oauth2',
        strategy: require('passport-github').Strategy,
        options: {
            clientID: 'cc31d5c4ce47394d7321',
            clientSecret: '5ab0e2c17b27186ba731347651eca841022f61d8',
            callbackURL: 'http://local.dynamiteseats.com/auth/github/callback'
        }
    },

    facebook: {
        name: 'Facebook',
        protocol: 'oauth2',
        strategy: require('passport-facebook').Strategy,
        options: {
            clientID: '317483115124558',
            clientSecret: '2085e6563e53b97129dd006909b68216',
            callbackURL: 'http://local.dynamiteseats.com/auth/facebook/callback',
            scope: ['email']
        }
    },

    google: {
        name: 'Google',
        protocol: 'oauth2',
        strategy: require('passport-google-oauth').OAuth2Strategy,
        options: {
            clientID: '16844144397-ilp4m00hs94kc7ct41naqdcoe55f5moh.apps.googleusercontent.com',
            clientSecret: 'VBmWDxuYiw7t4r6HRvykhILT',
            callbackURL: 'http://local.dynamiteseats.com/auth/google/callback',
            scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email']
        }
    }
};
