define(function (require) {

    var angular = require('angular'),
        Controllers = angular.module('controllers', ['ngSails']);

    Controllers.controller('GamesCtrl', require('controllers/gamesCtrl'));
    Controllers.controller('ImagesCtrl', require('controllers/imagesCtrl'));
    Controllers.controller('UsersCtrl', require('controllers/usersCtrl'));
    Controllers.controller('RegisterCtrl', require('controllers/registerCtrl'));
    Controllers.controller('StatsCtrl', require('controllers/statsCtrl'));

    return Controllers;

});