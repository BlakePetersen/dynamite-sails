define([
    'angular',
    'sails.io',
    'ng-sails',
    'controllers',
    'ui-bootstrap',
    'slick'
], function (angular, io) {

    var socket = io.connect(), app;

    socket.on('connect', function socketConnected() {
        console.log('Socket is now connected');
    });

    app = angular.module('dynamiteSeats', [

        'controllers',
        'ui.bootstrap'

    ]);

    return app;

});