window.name = 'NG_DEFER_BOOTSTRAP!';

require.config({

    'baseUrl': '/js',

    'paths': {
        'angular': '/js/dependencies/angular',
        'jquery': '/js/dependencies/jquery',
        'bootstrap': '/js/dependencies/bootstrap',
        'ui-bootstrap': '/js/dependencies/ui-bootstrap',
        'sails.io': '/js/dependencies/sails.io',
        'ng-sails': '/js/dependencies/angular-sails',
        'slick': '/js/dependencies/slick.min'

    },

    'shim': {
        'angular': {
            'exports': 'angular'
        },
        'jquery': {
            'exports': '$'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap/affix': {
            deps: ['jquery'],
            exports: '$.fn.affix'
        },
        'bootstrap/alert': {
            deps: ['jquery'],
            exports: '$.fn.alert'
        },
        'bootstrap/button': {
            deps: ['jquery'],
            exports: '$.fn.button'
        },
        'bootstrap/carousel': {
            deps: ['jquery'],
            exports: '$.fn.carousel'
        },
        'bootstrap/collapse': {
            deps: ['jquery'],
            exports: '$.fn.collapse'
        },
        'bootstrap/dropdown': {
            deps: ['jquery'],
            exports: '$.fn.dropdown'
        },
        'bootstrap/modal': {
            deps: ['jquery'],
            exports: '$.fn.modal'
        },
        'bootstrap/popover': {
            deps: ['jquery'],
            exports: '$.fn.popover'
        },
        'bootstrap/scrollspy': {
            deps: ['jquery'],
            exports: '$.fn.scrollspy'
        },
        'bootstrap/tab': {
            deps: ['jquery'],
            exports: '$.fn.tab'
        },
        'bootstrap/tooltip': {
            deps: ['jquery'],
            exports: '$.fn.tooltip'
        },
        'bootstrap/transition': {
            deps: ['jquery'],
            exports: '$.fn.transition'
        },
        'sails.io': {
            exports: 'io'
        },
        'ng-sails': {
            deps: ['angular','sails.io']
        },
        'ui-bootstrap': {
            deps: ['angular','bootstrap']
        },
        'slick':{
            deps: ['jquery']
        }
    }

});

require([
    'angular',
    'app',
    'bootstrap',
    'ui-bootstrap',
    'slick'
], function(angular, app) {

    //angular.element(document.getElementsByTagName('html')[0]);

    angular.element().ready(function() {
        angular.resumeBootstrap([app.name]);
    });

    $(document).ready(function() {

        $('.images-main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.images-thumb',
            infinite: true,
            lazyLoad: 'ondemand'
        });

        $('.images-thumb').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.images-main',
            dots: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true,
            lazyLoad: 'ondemand',
            infinite: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    });

});
