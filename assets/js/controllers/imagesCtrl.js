define(function () {

    return ['$scope', '$sails', function($scope, $sails) {

        $scope.getImageList = function() {

            $sails.get('/images/all').success(function(data) {

                convertDataObject(data);
                $scope.imageList = data;

            });

        };

        $scope.seatsZoom = function() {

            $('#seatsModal').modal();

        };

        $scope.viewParking = function() {

            $('#parkingModal').modal();

        };

        var convertDataObject = function (data) { for (var i = 0; i < data.length; i++) { data[i].index = i; } return data; };

    }]

});