define(function () {

    return ['$scope', '$sails', function($scope, $sails) {

        $scope.stats = {};

        $sails.get('/stats/standings/').success(function(data) {

            $scope.stats.standings = data;
            $('.stats [ng-controller]').removeClass('shroud');

        });

    }]

});