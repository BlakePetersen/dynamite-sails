define(function () {

    return ['$scope', '$sails', function($scope, $sails) {

        $scope.create = function() {

            $scope.register = $scope.register || {};
            $scope.register.provider = 'local';

            $sails.post('/auth/local/register', $scope.register).success(function(data) {

                if (data.errors) {
                    $scope.errors = data.errors;
                }

                if (data === 'success') {

                    document.location.href = '/';

                }

            });

        };

        $scope.login = function() {

            $scope.local = $scope.local || {};
            $scope.local.provider = 'local';

            $sails.post('/auth/local', $scope.local).success(function(data) {

                if (data.errors) {
                    $scope.errors = data.errors;
                }

                if (data === 'success') {

                    document.location.href = '/';

                }

            });

        };

    }];

});