define(function () {

    return ['$scope', '$sails', function($scope, $sails) {

        $sails.get('/games/all').success(function(data) {

            convertDataObject(data.games);
            $scope.games = data.games;
            $scope.user = data.user;

            $('ul.games').removeClass('shroud');

        });


        $scope.format = 'yyyy-MM-dd';
        $scope.minDate = $scope.minDate ? null : new Date();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        /* -- Methods -- */
        var convertDataObject = function (data) { for (var i = 0; i < data.length; i++) { data[i].index = i; } return data; };

        /* -- Angular -- */
        $scope.today = function(game) {

            var now = new Date().toJSON();
            return game.date > now;

        };


            $scope.alerts = [
                {type: 'success', msg: 'YO! In case you didn\'t know... All games come with TWO tickets'},
            ];


        $scope.closeAlert = function(index) {

            /* -- -- */
            $scope.alerts.splice(index, 1);

            /* -- Update user to never show again -- */
            if ($scope.user){

                $scope.user.alerts = {
                    twoTickets: true
                };

                $sails.post('/user/update/', $scope.user).success(function(data) {

                    console.log(data);

                }).error(function(err){

                    console.log(err);

                });

            }

        };

        $scope.availability = {sold:false};
        $scope.fireworks='';
        $scope.freeparking='';
        $scope.theme='';
        $scope.giveaway='';
        $scope.old = $scope.today;
        $scope.search='';
        $scope.vipPrice = 15;
        $scope.requestedGame = '';


        $scope.getGame = function(id) {

            $sails.get('/games/' + id).success(function(data) {

                convertDataObject(data);
                $scope.game = data;

            });

        };

        $scope.updateGame = function(id) {

            $sails.post('/games/' + id + '/update/', $scope.game).success(function() {

                document.location.href = '/';

            });

        };

        $scope.createGame = function() {

            $('#createModal').modal();

        };


        $scope.addGame = function () {

            $sails.post('/games/create', $scope.create).success(function(data) {

                data.index = $scope.games.length;

                $scope.games.push(data);

                $('#createModal').modal('hide');

            });

        };


        $scope.remove = function (data) {

            $sails['delete']('/games/' + data.id).success(function() {

                $scope.games.splice(data.index, 1);

            });

        };


        $scope.requestGame = function(game) {

            $scope.requestedGame = game;

            $scope.requestedGame.originalPrice = $scope.requestedGame.originalPrice || $scope.requestedGame.price;

            $('#requestModal').modal();

        };

        $scope.submitRequest = function(game) {

            $sails.post('/games/request', game).success(function(data) {

                $('#requestModal').modal('hide');

            });

        };

        $scope.vip = function() {

            if ($scope.requestedGame.vipParking) {

                $scope.requestedGame.price = +$scope.requestedGame.price + $scope.vipPrice;

            } else {

                $scope.requestedGame.price = $scope.requestedGame.originalPrice;

            }

        };


    }];

});