define(function () {

    return ['$scope', '$sails', function($scope, $sails) {

        $scope.update = function() {

            $sails.post('/user/update/', $scope.user).success(function(data) {

                if (typeof data == 'string') {
                    $scope.errors = 'The email address you entered, ' + $scope.user.email + ', is already being used by another account.';
                    $scope.user.email = '';
                }

            });

        };

    }];

});