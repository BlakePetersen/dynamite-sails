/**
 * HomeController
 *
 * @description :: Server-side logic for managing homes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Images = require('./ImagesController.js')

module.exports = {

    index: function (req, res) {

        var background = Images.random();

        res.view(null, {
            title: 'Home Schedule',
            background: background
        });

    }

};

