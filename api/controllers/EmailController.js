/**
 * EmailController
 *
 * @description :: Server-side logic for managing emails
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Mandrill = require('machinepack-mandrill');

var apiKey = 'WOUJ2uRAZP7LeqyyHYlB1A';
var fromEmail = 'noreply@dynamiteseats.com';
var fromName = 'Dynamite Seats';

function formatAMPM(date) {
    var hours = date.getHours() + 17;
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    return hours + ':' + minutes + ' ' + ampm;
}

module.exports = {

    confirm: function(data){

        data.date = new Date(data.date);
        data.time = new Date(data.time);

        var confirmationMessage = 'Hi ' + data.name + ',\n\n' +
            'Thank you for requesting the game on ' + data.date.toLocaleDateString() + ' at ' + formatAMPM(data.time) + ' against the ' + data.opponent + '. Once Ashley has confirmed availability, she will send you instructions with payment options. After payment has been received, the tickets can either be transferred to you through the A\'s website (account setup required) or can be mailed.\n\n\n' +
            'Thank you A\'s fans!\n\nAshley and Blake';

        Mandrill.sendPlaintextEmail({
            apiKey: apiKey,
            toEmail: data.email,
            toName: data.name,
            subject: 'Ticket Purchase Request Received',
            message: confirmationMessage,
            fromEmail: fromEmail,
            fromName: fromName
        }).exec({
            error: function (err){

            },
            success: function (){
                return data;
            }
        });

    },

    notify: function(data){

        data.date = new Date(data.date);
        data.time = new Date(data.time + 420);

        var notificationMessage = 'Name: <a href="mailto:' + data.email + '">' + data.name + '</a>' +
            '\n\nOpponent: ' + data.opponent +
            '\n\nDate: ' + data.date.toLocaleDateString() +
            '\n\nTime: ' + formatAMPM(data.time);

        if (data.vipParking) {

            notificationMessage +=    '\n\n VIP Parking: Yes';

        }

        notificationMessage += '\n\nPrice: $' + data.price +
            '\n\n\nPhone: ' + data.phone +
            '\n\nAddress: ' + data.address + ', ' + data.city + ', ' + data.state + ' ' + data.zip +
            //'\n\n\n<a href="http://www.dynamiteseats.com/games/' + data.id + '/confirm">Confirm Request</a>' +
            //'\n<a href="http://www.dynamiteseats.com/games/' + data.id + '/deny">Deny Request</a>' +
            '\n<a href="http://www.dynamiteseats.com/games/' + data.id + '/edit">Edit Game</a>';

        Mandrill.sendPlaintextEmail({
            apiKey: apiKey,
            toEmail: 'ashleydawnpetersen@gmail.com',
            //toEmail: 'blakepetersen@gmail.com',
            toName: 'Ashley Petersen',
            subject: 'Ticket Purchase Request Notification - ' + data.opponent + ' on ' + data.date.toLocaleDateString(),
            message: notificationMessage,
            fromEmail: fromEmail,
            fromName: fromName
        }).exec({
            error: function (err){

            },
            success: function (){
                return data;
            }
        });

    }

};

