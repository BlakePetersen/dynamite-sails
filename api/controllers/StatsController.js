/**
 * StatsController
 *
 * @description :: Server-side logic for managing stats
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var request = require('request');
var _ = require('underscore');
var Agenda = require('../jobs/agenda');

var convertStats = function(data, type) {

    var results = {

        rank: 0,
        won: 0,
        lost: 0,
        streakType: '',
        streakTotal: 0,
        gamesBack: 0,
        gamesPlayed: 0,
        winPercentage: 0

    };

    if (type == 'standings') {

        data = _.findWhere(data.standing, {team_id: 'oakland-athletics'});

        results.rank = data.rank;
        results.won = data.won;
        results.lost = data.lost;
        results.streakType = data.streak_type;
        results.streakTotal = data.streak_total;
        results.gamesBack = data.games_back;
        results.gamesPlayed = data.games_played;
        results.winPercentage = data.win_percentage;

    }

    return results;

};

module.exports = {

    refresh: function(req, res) {

        Agenda.every('2 hours', 'refresh');

        return res.ok();

    },

    get: function(req, res){

        var options = {
            url: 'https://erikberg.com/mlb/standings.json',
            headers: {
                'User-Agent': 'DynamiteStats/0.1 (blakepetersen@gmail.com)'
            }
        };

        function callback(error, response, body) {

            if (!error && response.statusCode == 200) {

                var info = JSON.parse(body);

                info = convertStats(info, 'standings');

                Stats.find().exec(function(err, stats){

                    if(stats.length == 0){

                        Stats.create(info).exec(function(){

                          return res.ok();

                        });

                    } else {

                        Stats.update(stats._id, info).exec(function(){

                          return res.ok();

                        });

                    }

                });


            }

        }

        request(options, callback);

    },

    standings: function(req, res) {

        Stats.find().exec(function(err, stats){

            return res.json(stats[0]);

        });

    }

};

