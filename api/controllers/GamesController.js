/**
 * GamesController
 *
 * @description :: Server-side logic for managing games
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Email = require('./EmailController.js');

module.exports = {

    index: function (req, res) {

        res.view(null, {
            title: 'Games'
        });

    },

    edit: function (req, res) {

        Games.findOne(req.params.id).exec(function(err, game) {

            console.log(game);

            res.view(null, {
                title: 'Edit',
                game: game,
                user: req.user
            });

        });

    },

    all: function(req, res) {

        Games.find().exec(function(err, games) {

            return res.json({games: games, user: req.user});

        });

    },

    create: function(req, res) {

        Games.create(req.body).exec(function(err, game) {

            if (err) { return res.json(err) }

            return res.json(game);
        });

    },

    update: function(req, res) {

        Games.update(req.params.id, req.body).exec(function(err) {

            return res.json(req.body);

        });

    },

    request: function(req, res) {

        var transport = {};

        // User Info
        transport.name = req.user.name;
        transport.email = req.user.email;
        transport.phone = req.user.phone;
        transport.address = req.user.address;
        transport.city = req.user.city;
        transport.state = req.user.state;
        transport.zip = req.user.zip;

        // Game Info
        transport.id = req.body.id;
        transport.opponent = req.body.opponent;
        transport.date = req.body.date;
        transport.time = req.body.time;

        if (req.body.vipParking) { transport.vipParking = true; }

        transport.price = req.body.price;

        Email.confirm(transport);
        Email.notify(transport);

        return res.json(transport);

    }

};

