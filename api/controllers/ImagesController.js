/**
 * ImagesController
 *
 * @description :: Server-side logic for managing games
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');

module.exports = {

    query: function() {

        return fs.readdirSync('./assets/images/seats');

    },

    random: function () {

        var images = module.exports.query();
        return images[Math.floor(Math.random()*images.length)];;

    },

    all: function(req, res) {

        var images = module.exports.query();

        return res.json(images);

    }

};

