/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    update: function(req, res) {

        User.update(req.body.id, req.body).exec(function(err, data) {

            if (err){
                return res.send(err.code);
            } else {
                return res.json(data);
            }


        });

    }

};

