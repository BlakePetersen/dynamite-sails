/**
 * Games.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var Games = {

    schema: true,

    attributes: {

        date: {
            type: 'Date',
            required: true
        },
        time: {
            type: 'String',
            required: true
        },
        requested: {
            type: 'Boolean',
            defaultsTo: false
        },
        sold: {
            type: 'Boolean',
            defaultsTo: false
        },
        paid: {
            type: 'Boolean',
            defaultsTo: false
        },
        price: {
            type: 'String'
        },
        fireworks: {
            type: 'Boolean',
            defaultsTo: false
        },
        opponent: {
            type: 'String',
            required: true
        },
        theme: {
            type: 'String'
        },
        giveaway: {
            type: 'String'
        },
        special: {
            type: 'String'
        },
        comments: {
            type: 'String',
            trim: true
        },
        buyer: {
            type: 'String'
        }

    }

};

module.exports = Games;
