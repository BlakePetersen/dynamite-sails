/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var User = {

    schema: true,

    attributes: {

        'username': { 'type': 'string', 'unique': true, 'required': true },
        'email': { 'type': 'email',  'unique': true,  'required': true },
        phone: { type: 'string',  required: true },
        name: { type: 'string', required: true },
        address: { type: 'string', required: true },
        city: { type: 'string', required: true },
        state: { type: 'string', required: true },
        zip: { type: 'string', required: true },
        alerts: { type: 'json', defaultsTo: {} }

    }

};

module.exports = User;
