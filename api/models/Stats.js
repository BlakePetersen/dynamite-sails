module.exports = {

    attributes: {


        rank: {
            type: 'Integer'
        },
        won: {
            type: 'Integer'
        },
        lost: {
            type: 'Integer'
        },
        streakType: {
            type: 'String'
        },
        streakTotal: {
            type: 'Integer'
        },
        gamesBack: {
            type: 'String'
        },
        gamesPlayed: {
            type: 'Integer'
        },
        winPercentage: {
            type: 'String'
        }

    }

};

