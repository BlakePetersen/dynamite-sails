
module.exports = function(agenda) {

    agenda.define('refresh', function (job, done) {

        var stats = require('../controllers/StatsController.js');

        stats.get();

        done();

    });

};