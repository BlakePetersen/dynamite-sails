var Agenda = require('agenda');

var connectionOpts = {
    db: {address: 'localhost:27017/dynamite-seats'}
};

var agenda = new Agenda(connectionOpts);

require('../jobs/item')(agenda);

agenda.start();

module.exports = agenda;